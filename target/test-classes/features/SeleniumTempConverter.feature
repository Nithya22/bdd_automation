Feature: This feature verify to converting Centigrade to Fahrenheit Converter through webpage

@HappyPath
Scenario Outline: This scenario validates the fahrenheit value
Given User launches the convert temperature site and enter <Centigrade1>
When clicked on convert
Then Verify the value of Fahrenheit <Fahrenheitvalue1> through the website


Examples:
|Centigrade1|Fahrenheitvalue1|
|  68      |       154.4   |
|  68      |       150.0   |


