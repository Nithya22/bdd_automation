Feature: This feature is to search the github repository using a keyword

@HappyPath
Scenario Outline: This scenario searches the github repository with a keyword
Given url is <url>
And User enters the <Keyword> for searching the github api
When the api is triggered
Then Verify the HTTP response <code>

Examples:
|url											   |Keyword    |code |
|  https://api.github.com/search/repositories      |  angular4 | 200 |
|  https://api.github.com/search/repositories      |   java    | 200 |