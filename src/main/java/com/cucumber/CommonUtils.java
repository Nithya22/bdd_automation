package com.cucumber;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class CommonUtils {
	
	
	public static WebDriver driver;
		
	public float CentigradeToFahrenheit(int cel)
	{			
	  
	  float far;
	  far = cel * 9/5 + 32;
	  return far;	  
	}
	
	
		public static void initializeBrowser() {

	try {
		String path=System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver",path+"\\driver\\chromedriver.exe");
											
		    	driver = new ChromeDriver();
		    	driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
		    	driver.get("https://www.rapidtables.com/convert/temperature/celsius-to-fahrenheit.html");
		    	driver.manage().window().maximize();
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
		}
	
	public static void enterValue(String keysToSend){
		driver.findElement(By.xpath("//input[@name=\"x\"]")).sendKeys(keysToSend);
	}
	public static void click() {
		driver.findElement(By.xpath("//button[@title=\"Convert\"]")).click();
	}
				
	
	public static String getValue() {
		
		String faren=driver.findElement(By.xpath("//input[@name=\"y\"]")).getAttribute("value");
		System.out.println("Frenheit value from Web Site " + faren);
		driver.quit();
		return faren;

	}
}