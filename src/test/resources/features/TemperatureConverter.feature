Feature: This feature verify to converting Centigrade to Fahrenheit Converter through java code

@HappyPath
Scenario Outline: This scenario validates the fahrenheit value
Given User enter the <Centigrade> data for converting to fahrenheit
When triggered to convert
Then Verify the Fahrenheit value <Fahrenheitvalue> 


Examples:
|Centigrade|Fahrenheitvalue|
|  68      |       154.0   |
|  68      |       150.0   |