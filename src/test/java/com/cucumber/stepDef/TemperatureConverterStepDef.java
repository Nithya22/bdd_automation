package com.cucumber.stepDef;

import org.testng.Assert;

import com.cucumber.CommonUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TemperatureConverterStepDef {
	CommonUtils CU= new CommonUtils();
	int Centigradevalue;
	float getFahrenvalue;
	
	@Given("User enter the (.*) data for converting to fahrenheit")
	public void enterData(int Centigrade) {
		this.Centigradevalue=Centigrade;
	}
	@When("triggered to convert")
	public void trigger()
	{
		getFahrenvalue=CU.CentigradeToFahrenheit(Centigradevalue);
		System.out.println("Centigrade Value Passed is : "+Centigradevalue);
	}
	@Then("Verify the Fahrenheit value (.*)")
	public void verifyConversion(float Fahrenheitvalue1 ) {
		try {
		Assert.assertEquals(getFahrenvalue, Fahrenheitvalue1);
		System.out.println("PASS :: Fahrenheit Value calculated "+ getFahrenvalue+" and Fahreneit Value from Feature File: "+Fahrenheitvalue1+" are Matching");

		}catch(AssertionError e) {
		System.out.println("FAIL :: Fahrenheit Value calculated "+ getFahrenvalue+" and Fahreneit Value from Feature File: "+Fahrenheitvalue1+" are Not Matching");
		}
	}

} 