package com.cucumber.stepDef;

import java.io.IOException;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



public class SearchGitHubStepDef {
	static String restUrl;
	static int code;
	static String searchKeyword;

	@Given("url is (.*)")
	public static void setUrl(String url) {
		restUrl = url;
	}

	@Given("User enters the (.*) for searching the github api")
	public static void setSearchKeyword(String keyword) {
		searchKeyword = keyword;
	}

	@When("the api is triggered")
	public static void makeRESTCall() throws Exception {
		restUrl = restUrl + "?q=" + searchKeyword;
		HttpClient httpclient = HttpClientBuilder.create().build();

		URIBuilder builder = new URIBuilder(restUrl);
		URI uri = builder.build();

		HttpGet get = new HttpGet(uri);

		try {
			HttpResponse response = httpclient.execute(get);
			code = response.getStatusLine().getStatusCode();
			System.out.println("Searching github repos using keyword " + searchKeyword + ", code received is " + code);
		} catch (IOException e) {
			System.out.println("Exception thrown while getting response");
			throw new Exception("Exception thrown while getting response");
		}

	}

	@Then("Verify the HTTP response (.*)")
	public static void verifyResponse(int statusCode) {
		try {
			Assert.assertEquals(statusCode, code);
		} catch (AssertionError e) {
			System.out.println("FAIL : Expexted was: " + statusCode + "Actual is: " + code);
		}
	}
}
