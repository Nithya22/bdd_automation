package com.cucumber.stepDef;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.cucumber.CommonUtils;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SeleniumTempConverterStepDef {
	static WebDriver driver;
	
	
	@Given("User launches the convert temperature site and enter (.*)")
	public void enterCentigrade(String Centigrade1) {
		CommonUtils.initializeBrowser();
		CommonUtils.enterValue(Centigrade1);
	}
	
	@When("clicked on convert")
	public void clickConvert() {
		CommonUtils.click();
	}

	@Then ("Verify the value of Fahrenheit (.*) through the website")
	public void value(float compareWith) {
		String Actual=CommonUtils.getValue();
		Float floatActual = Float.valueOf(Actual);
		
		try {
		Assert.assertEquals(compareWith, floatActual,0);
		System.out.println("PASS :: The Fahrenheit Value from Web Site: " +Actual+ " matched the value passed in Feature File: " + compareWith);
		
		} catch(AssertionError e) {
		System.out.println("FAIL :: The Fahrenheit Value from Web Site: "+ Actual +" and the value passed in Feature File: "+compareWith+" Not matching");
		}
	}
	
	
}
